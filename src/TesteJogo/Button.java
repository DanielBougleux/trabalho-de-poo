package TesteJogo;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.SlickException;

public class Button implements Clicavel{

    private Image[] imagens;
    private float coordx;
    private float coordy;
    private int imgatual;
    private int limImg;

    public Button(String[] imagem, float coordx, float coordy) throws SlickException {
        imagens = new Image[imagem.length];
        for(int i = 0; i<imagem.length; i++){
            imagens[i] = new Image(imagem[i]);
        }
        this.coordx = coordx;
        this.coordy = coordy;
        limImg = imagem.length-1;
        imgatual = 0;
    }

    public void draw(Graphics g){
        g.drawImage(imagens[imgatual], coordx, coordy);
    }


    public boolean clicou(int mousex, int mousey){
        if(verificaMouse(mousex,mousey)){
            return Mouse.isButtonDown(0);
        }
        return false;
    }

    public boolean verificaMouse(int mousex, int mousey){
        return (((mousex > coordx) && (mousex < (coordx + imagens[imgatual].getWidth()))) &&
                ((mousey > coordy) && (mousey < (coordy + imagens[imgatual].getHeight()))));
    }


    public float getCoordx() {
        return coordx;
    }

    public void setCoordx(float coordx) {
        this.coordx = coordx;
    }

    public float getCoordy() {
        return coordy;
    }

    public void setCoordy(float coordy) {
        this.coordy = coordy;
    }

    public void setimgatual(int imgatual){
        this.imgatual = imgatual;
    }

    public float getHeight(){
        return imagens[imgatual].getHeight();
    }

    public float getWidth(){
        return imagens[imgatual].getWidth();
    }

    public int getimgatual(){
        return imgatual;
    }


}
