package TesteJogo;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;


public class Main {

    private static final String gamename = "Asteroids";

    public static void main(String[] args) {

        AppGameContainer appgc;
        try{
            appgc = new AppGameContainer(new Jogo(gamename));
            appgc.setDisplayMode(800,600,false);
            appgc.start();
        }catch (SlickException e){
            e.printStackTrace();
        }

    }
}
