package TesteJogo;

import java.io.FileNotFoundException;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

import java.io.RandomAccessFile;    
import java.io.IOException;


public class Recordes extends BasicGameState{
    String[] linha;
    RandomAccessFile arq;
    long inicio_arq;
    int x, y;
    int height = 720;
    private int ID;
    
    public Recordes(int state){
        ID = state;
    }
    
    public void init(GameContainer gc, StateBasedGame estado) throws SlickException{
        x = 100;
        y = 50;
        try {
            arq = new RandomAccessFile("src/Game1/recordes.txt", "r");
            x = 100;
            y = 50;
            linha = new String[10];
            for(int i = 0; i < 10; i++){
                linha[i] = arq.readLine();
            }
            arq.close();
        }
        catch (FileNotFoundException ex) {
            System.out.print(ex);
        }
        catch (IOException ex) {
            System.out.print(ex);
        }       
    }
    
    public void render(GameContainer gc, StateBasedGame estado, Graphics g) throws SlickException{     
        g.drawString("Recordes", x, y-20);
                
        for(int i = 0; i < 10; i++){

                g.drawString(linha[i], x, y+i*15);
            }
        g.drawRect(x, y+160, 250, 40);
        g.drawString("Pressione ESC para voltar ao menu principal", x+10, y+170);
    }
    
    
    
    public void update(GameContainer gc, StateBasedGame estado, int delta) throws SlickException{
        Input entrada = new Input(height);
        int pos_x = entrada.getMouseX();
        int pos_y = entrada.getMouseY();
        if((pos_x >=100 && pos_x <= 350 ) &&
           (pos_y >= 210 && pos_y <= 250) &&
           (entrada.isMouseButtonDown(0) == true)){
            //MUDAR DE TELA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //this.enterState(id menu principal);
        }
        
    }
    
    public int getID(){
        return ID;
    }
    
}

