package TesteJogo;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

public class Controles extends BasicGameState{

    private int ID;
    private Image background;
    private float imageWidth;
    private float imageHeight;
    private int nImages;
    private Image[] controles;

    public Controles(int state, float imageWidth, float imageHeight, int nImages){
        ID = state;
        this.imageHeight = imageHeight;
        this.imageWidth = imageWidth;
        this.nImages = nImages;
    }

    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
        background = new Image("Images/Planos de Fundo/PlanoControles.jpg");
        controles = new Image[nImages];
        for(int i = 0; i<nImages; i++){
            controles[i] = new Image("Images/Controles/Img" + (i+1) + ".png");
        }
    }

    public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{
        background.draw((gc.getWidth()-background.getWidth())/2, (gc.getHeight()-background.getHeight())/2);
        float expaçamentox = (gc.getWidth() - imageWidth)/2;
        float espaçamentoy = (gc.getHeight() - imageHeight * nImages)/(nImages+1);
        for (int i = 0; i<nImages; i++){
            g.drawImage(controles[i], expaçamentox, espaçamentoy*(i+1) + imageHeight * i);
        }
    }

    public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException{
        Input input = gc.getInput();
        if(input.isKeyDown(Input.KEY_ESCAPE)){
            sbg.enterState(0);
        }
    }

    public int getID(){
        return ID;
    }

}
