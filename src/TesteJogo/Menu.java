package TesteJogo;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;
import org.lwjgl.input.Mouse;

public class Menu extends BasicGameState {

    private int ID;
    private Clicavel[] botoes;
    private float botoesHeight;
    private float botoesWidth;
    private int nBotoes;
    private Image background;

    public Menu(int state, float botoesHeight, float botoesWidth, int nBotoes){

        ID = state;
        this.botoesHeight = botoesHeight;
        this.botoesWidth = botoesWidth;
        this.nBotoes = nBotoes;
        botoes = new Clicavel[nBotoes];

    }

    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {

        // calcula o espaçamento dos botões em y. A fórmula eh:
        // (Altura da janela - altura dos botões * numero de botões)/número de botões +1
        float espacamentoy = (gc.getHeight() - botoesHeight * nBotoes)/(nBotoes+1);
        // Calcula o espaçamento dos botões em x. A formula eh: (Largura da janela - largura dos botões)/ 2;
        float espacamentox = (gc.getWidth() - botoesWidth)/2;
        String[] novojogo = {"Images/Botões/NovoJogoSH.png", "Images/Botões/NovoJogoCH.png"};
        botoes[0] = new Button(novojogo, espacamentox, espacamentoy);
        String[] naves = {"Images/Botões/NavesSH.png", "Images/Botões/NavesCH.png"};
        botoes[1] = new Button(naves, espacamentox, espacamentoy *2 + botoesHeight);
        String[] ranking = {"Images/Botões/RankingSH.png", "Images/Botões/RankingCH.png"};
        botoes[2] = new Button(ranking, espacamentox, espacamentoy*3 + botoesHeight*2);
        String[] controles = {"Images/Botões/ControlesSH.png", "Images/Botões/ControlesCH.png"};
        botoes[3] = new Button(controles, espacamentox, espacamentoy *4+ botoesHeight*3);
        String[] salvarJogo = {"Images/Botões/SalvarJogoSH.png", "Images/Botões/SalvarJogoCH.png"};
        botoes[4] = new Button(salvarJogo, espacamentox,espacamentoy*5+botoesHeight*4);
        String[] sair = {"Images/Botões/SairSH.png", "Images/Botões/SairCH.png"};
        botoes[5] = new Button(sair,espacamentox, espacamentoy*6+botoesHeight*5);
        background = new Image("Images/Planos de Fundo/PlanoMenu.jpeg");

    }

    public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{

        background.draw((gc.getWidth()-background.getWidth())/2, (gc.getHeight()-background.getHeight())/2);
        for(Clicavel x: botoes){
            x.draw(g);
        }

    }

    public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException{

        int mousex = Mouse.getX();
        int mousey = Math.abs(gc.getHeight()-Mouse.getY());

        for (Clicavel x: botoes){
            if(x.verificaMouse(mousex,mousey)){
                x.setimgatual(1);
            }else {
                x.setimgatual(0);
            }
        }

        if(botoes[0].clicou(mousex,mousey)){
            sbg.enterState(4);
        }else if(botoes[1].clicou(mousex,mousey)){
            Naves temp = (Naves)sbg.getState(3);
            temp.readInfo();
            sbg.enterState(3);
        }else if(botoes[2].clicou(mousex,mousey)){
            sbg.enterState(2);
        }else if(botoes[3].clicou(mousex,mousey)){
            sbg.enterState(5);
        }else if(botoes[4].clicou(mousex,mousey)){
            //logica de salvar o jogo;
        }else if(botoes[5].clicou(mousex,mousey)){
            System.exit(0);
        }

    }

    public int getID(){
        return ID;
    }

}
