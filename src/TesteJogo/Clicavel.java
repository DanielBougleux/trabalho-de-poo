package TesteJogo;

import org.newdawn.slick.Graphics;

public interface Clicavel extends Desenhavel {

    public boolean clicou(int mousex, int mousey);
    public boolean verificaMouse(int mousex, int mousey);
    public float getHeight();
    public float getWidth();
    public void draw(Graphics g);
    public int getimgatual();
    public void setimgatual(int imgatual);
}
