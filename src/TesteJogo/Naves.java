package TesteJogo;

import org.lwjgl.Sys;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.util.Scanner;

public class Naves extends BasicGameState{

    private int ID;
    private int[] booleanNaves;
    private int nNaves;
    private int gold;
    private int[] precoNaves;
    private Clicavel[] Bnaves;
    private Clicavel[] Bsetas;
    private float navesWidth;
    private float navesHeight;
    private float setasWidth;
    private float setasHeight;
    private int naveatual;
    private boolean isClicked0;
    private boolean isClicked1;
    private Image background;
    private Image goldError;
    private boolean showGoldError;
    private long tempoFim;
    private long tempoIni;
    private boolean reinit;

    public Naves(int state ,int nNaves, float navesWidth, float navesHeight
    , float setasWidth, float setasHeight, int[] precoNaves){

        ID = state;
        this.booleanNaves = new int[nNaves];
        this.nNaves = nNaves;
        this.setasHeight = setasHeight;
        this.setasWidth = setasWidth;
        this.navesHeight = navesHeight;
        this.navesWidth = navesWidth;
        Bnaves = new Button[nNaves];
        Bsetas = new Button[2];
        naveatual = 0;
        this.precoNaves = precoNaves;
        showGoldError = false;

    }

    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {

        float espacamentox = (gc.getWidth()-navesWidth-setasWidth*2)/4;
        float espacamentoynaves = (gc.getHeight()-navesHeight)/2;
        float espacamentoysetas = (gc.getHeight()-setasHeight)/2;

        String[] nave1 = {"Images/TelaDeNaves/TelaNave1CC.png", "Images/TelaDeNaves/TelaNave1SC.png"};
        Bnaves[0] = new Button(nave1, espacamentox*2+setasWidth, espacamentoynaves);
        String[] nave2 = {"Images/TelaDeNaves/TelaNave2CC.png", "Images/TelaDeNaves/TelaNave2SC.png"};
        Bnaves[1] = new Button(nave2, espacamentox*2+setasWidth, espacamentoynaves);
        String[] nave3 = {"Images/TelaDeNaves/TelaNave3CC.png", "Images/TelaDeNaves/TelaNave3SC.png"};
        Bnaves[2] = new Button(nave3, espacamentox*2+setasWidth, espacamentoynaves);

        int i;
        for(i = 0; i<nNaves; i++){
            if(booleanNaves[i] == 0){
                Bnaves[i].setimgatual(1);
            }
        }

        String[] seta1 = {"Images/TelaDeNaves/BotãoSetaDNC.png", "Images/TelaDeNaves/BotãoSetaDC.png"};
        Bsetas[0] = new Button(seta1, espacamentox*3+setasWidth+navesWidth, espacamentoysetas);
        String[] seta2 = {"Images/TelaDeNaves/BotãoSetaENC.png", "Images/TelaDeNaves/BotãoSetaEC.png"};
        Bsetas[1] = new Button(seta2, espacamentox, espacamentoysetas);

        isClicked0 = false;
        isClicked1 = false;

        background = new Image("Images/Planos de Fundo/PlanoNaves.jpg");
        goldError = new Image("Images/TelaDeNaves/GoldInsuficiente.png");

        readInfo();

        //reinit = true;

    }

    public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{

        background.draw((gc.getWidth()-background.getWidth())/2,
                (gc.getHeight()-background.getHeight())/2);

        Bnaves[naveatual].draw(g);

        Bsetas[0].draw(g);
        Bsetas[1].draw(g);

        g.drawString("Dinheiro: "+ gold, 10,50);

        if(showGoldError){
            tempoFim = System.currentTimeMillis();
            if(tempoFim-tempoIni >= 1000){
                showGoldError = false;
            }
            goldError.draw((gc.getWidth()-goldError.getWidth())/2,
                    (gc.getHeight()-goldError.getHeight())/2);
        }

    }

    public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException{

        /*if(reinit){
            this.readInfo();
        }*/

        //reinit = false;

        Input input = gc.getInput();
        if(input.isKeyDown(Input.KEY_ESCAPE)){
            saveInfo();
            reinit = true;
            sbg.enterState(0);
        }

        int mousex = Mouse.getX();
        int mousey = Math.abs(gc.getHeight()-Mouse.getY());

        if(Bnaves[naveatual].clicou(mousex,mousey) && booleanNaves[naveatual] == 0){
            if(gold > precoNaves[naveatual]){
                gold -= precoNaves[naveatual];
                booleanNaves[naveatual] = 1;
                Bnaves[naveatual].setimgatual(0);
            }else {
                showGoldError = true;
                tempoIni = System.currentTimeMillis();
            }
        }

        if(Bsetas[0].clicou(mousex, mousey)){
            Bsetas[0].setimgatual(1);
            isClicked0 = true;
        }else{
            Bsetas[0].setimgatual(0);
            if(isClicked0){
                if(naveatual == nNaves-1){
                    naveatual = 0;
                }else {
                    naveatual++;
                }
            }
            isClicked0 = false;
        }

        if(Bsetas[1].clicou(mousex, mousey)){
            Bsetas[1].setimgatual(1);
            isClicked1 = true;
        }else {
            Bsetas[1].setimgatual(0);
            if(isClicked1){
                if(naveatual == 0){
                    naveatual = nNaves-1;
                }else {
                    naveatual--;
                }
            }
            isClicked1 = false;
        }

    }

    public int getID(){
        return ID;
    }

    public void readInfo(){

        InputStream is;

        try{
            is = new FileInputStream("Arquivos/Infos.txt");
        }catch (FileNotFoundException f){
            System.out.println("Erro ao abrir o arquivo: Arquivo não encontrado");
            return;
        }

        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String s;
        try {
            s = br.readLine();
            String[] spl = s.split(",");
            int i;
            for(i = 0; i < nNaves; i++){
                booleanNaves[i] = Integer.parseInt(spl[i]);
            }
            gold = Integer.parseInt(spl[i]);
            br.close();
        }catch (IOException i) {
            System.out.println("Erro ao ler do arquivo");
            return;
        }
    }

    public void saveInfo(){

        OutputStream os;

        try{
            os = new FileOutputStream("Arquivos/Infos.txt");
        }catch (FileNotFoundException e){
            System.out.println("Arquivo não encontrado");
            return;
        }
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);

        try{
            for(int x: booleanNaves) {
                bw.write(x + ",");
            }
            bw.write("" + gold);
            bw.close();
        }catch (IOException i){
            System.out.println("Erro ao salvar no arquivo");
            System.exit(1);
        }
    }

}
