package TesteJogo;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

public class SelectNaves extends BasicGameState{

    private int ID;

    public SelectNaves(int state){
        ID = state;
    }

    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {

    }

    public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException{
        g.drawString("Bem vindo a tela de seleção de naves\nPressione Esc Para retornar ao menu", 100, 100);
    }

    public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException{
        Input input = gc.getInput();
        if(input.isKeyDown(Input.KEY_ESCAPE)){
            sbg.enterState(0);
        }
    }

    public int getID(){
        return ID;
    }

}
