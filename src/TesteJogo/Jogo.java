package TesteJogo;


import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

public class Jogo extends StateBasedGame {

    private final int menu = 0;
    private final int play = 1;
    private final int rankings = 2;
    private final int naves = 3;
    private final int selectnaves = 4;
    private final int controles = 5;

    public Jogo(String gamename){
        super(gamename);
        this.addState(new Menu(menu, 50, 300, 6));
        this.addState(new Play(play));
        this.addState(new Recordes(rankings));
        int[] precoNaves = {1000,2000,3000};
        this.addState(new Naves(naves,3,450, 342, 75,75, precoNaves ));
        this.addState(new SelectNaves(selectnaves));
        this.addState(new Controles(controles, 512, 50, 5));
    }

    public void initStatesList(GameContainer gc)throws SlickException{
        this.getState(menu).init(gc, this);
        this.getState(play).init(gc, this);
        this.getState(rankings).init(gc,this);
        this.getState(naves).init(gc,this);
        this.getState(selectnaves).init(gc, this);
        this.getState(controles).init(gc, this);
        this.enterState(menu);
    }



}
